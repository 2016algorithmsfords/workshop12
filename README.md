I am sorry for slamming Python's optimization routines. They are definitely not as extensive as the ones available for other languages (Matlab, C++, etc.) 

My major issues were that: 
- I did not see a quadratic programming algorithm. (Using SLSQP will expose QP. So, I was wrong.)
- I did not see a way to have non-linear constraints in the optimization. ("constraints" are poorly documented, but one can do so)
- The way constraints are specified is not standard. 

Thus, there is enough there to work with. Though, one needs to use SLSQP when using constrained optimization. This is not that big a deal because it is still one of the better algorithms when using constraints. 

In any case, if you are optimizing without constraints, use any of the methods we covered. If you need to use constraints, your best bet is SLSQP. See, optimize.pyPreview the documentView in a new window for some ideas on how to use the optimizer.

1 i) What is the difference between optimization 1a and optimization 1b?
1 ii) What is the difference between optimizations 1b and 2?
1 iii) Describe the problem optimization 3 tries to solve?
1 iv) What is the difference between optimizations 3 and 4? Which one is better, and why?
1 v) What is an improvement to optimization 4 that one may want to add to allow the optimizer to solve the same problem even faster?

2 Use python's optimization kit (like in optimize.py) to perform any of the optimizations you were asked to do for the homework.

Submit all your work at: https://bitbucket.org/2016algorithmsfords/workshop12

------------------- optimize.py -------------------
from scipy import optimize as opt
import numpy as np

def weird(x):
    return x[0]**2 - 12 + x[1]**3
def weird2(x):
    return x[0]**2+x[1]**4-x[0]-0.65*x[1]

# Optimizations 1a and 1b
print("Optimization 1a and Optimization 1b")
con = {"type": 'ineq', 'fun': lambda x: -x[0] - x[1] + 1}
bnd = ((0,None), (0,None))
print(opt.minimize(weird2, [0,0], method='SLSQP'))
print()
print(opt.minimize(weird2, [0,0], method='SLSQP', bounds=bnd, constraints=con))

# Optimizations 2
print("\nOptimization 2")
con2 = ({"type": 'ineq', 'fun': lambda x: -x[0] - x[1] + 1},
       {"type": 'ineq', 'fun': lambda x:  x[1]} ,
       {"type": 'ineq', 'fun': lambda x:  x[0]} )
print(opt.minimize(weird2, [0,0], method='SLSQP', constraints=con2))

# Optimizations 3
print("\nOptimization 3")
con3 = ({"type": 'eq', 'fun': lambda x:  x[0]*x[0] + x[1]*x[1] - 1} )
print(opt.minimize(weird2, [10,0], method='SLSQP', constraints=con3))

# Optimizations 4
print("\nOptimization 4")
con4 = ({"type": 'eq', 'fun': lambda x:  x[0]*x[0] + x[1]*x[1] - 1, 'jac': lambda x: np.array([2*x[0], 2*x[1]])} )
print(opt.minimize(weird2, [10,0], method='SLSQP', constraints=con4))

